public class Compile {
    public static void main(String[] arg) {
        Compiler compiler = new Compiler();
        compiler.compile(arg[0]);
        System.out.println(compiler.errors + " errors detected");
    }
}