main {
  int a;
  a <- 2;
  if (5 < a) then
  {
    a <- 1;
  }
  if (!(5 < a)) then
  {
    a <- 0;
  }
  print(a);
  return a;
}

