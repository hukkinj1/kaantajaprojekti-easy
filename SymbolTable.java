import java.util.Map;
import java.util.HashMap;

class Symbol {
    public String name;
    public int type;
    public int address;
}

public class SymbolTable {
    Map<String, Symbol> map;
    int nextSymbolAddress;

    public SymbolTable() {
        map = new HashMap<String, Symbol>();
        nextSymbolAddress = 0;
    }

    public void add(String name, int type) {
        Symbol newSymbol = new Symbol();
        newSymbol.name = name;
        newSymbol.type = type;
        newSymbol.address = nextSymbolAddress;
        nextSymbolAddress++;

        map.put(newSymbol.name, newSymbol);
    }

    public Symbol find(String name) {
        return map.get(name);
    }
}