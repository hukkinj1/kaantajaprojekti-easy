import fi.tkk.cs.tkkcc.slx.*;

public class CodeGenerator {
    private final SlxProgram program;
    private int nextLabel;

    public CodeGenerator() {
        program = new SlxProgram();
        nextLabel = 0;
    }



    public void emit(final CommandWord command) {
        program.emit(command);
    }

    public void emit(final CommandWord command, final int param1) {
        program.emit(command, param1);
    }

    public void emit(final CommandWord command, final int param1, final int param2) {
        program.emit(command, param1, param2);
    }


    public String toString() {
        return program.toString();
    }

    public SlxProgram getProgram() {
    	return program;
    }

    public int getNewLabel() {
        return nextLabel++;
    }
}
