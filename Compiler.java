import fi.tkk.cs.tkkcc.SlxCompiler;
import fi.tkk.cs.tkkcc.slx.*;

public class Compiler implements SlxCompiler {

    private Scanner scanner;
    private Parser parser;
    public int errors;

    public Compiler() {
        errors = 0;
    }

    public boolean isErrors() {
        if (errors > 0) {
            return true;
        }
        return false;
    }

    public SlxProgram compile(String sourceFilename) {
        scanner = new Scanner(sourceFilename);
        parser = new Parser(scanner);
        parser.symbols = new SymbolTable();
        parser.gen = new CodeGenerator();

        parser.Parse();

        errors = parser.errors.count;

        // DEBUG: print SLX code
        //System.out.println("\nprogram.toString():");
        //System.out.println(parser.gen.toString() + "\n");

        return parser.gen.getProgram();
    }
}